package com.epam.query;

public class UserSqlQueries {
    public static final String SELECT_FROM_USER_BY_ID = "SELECT u.id, u.first_name, u.last_name, u.email, \n" +
            "u.password, u.phone,r.id as role_id, r.name as role FROM user as u\n" +
            "join role as r on u.role_id = r.id\n" +
            "where u.id = ?";
    public static final String SELECT_FROM_USER_BY_EMAIL = "SELECT u.id, u.first_name, u.last_name, u.email," +
            " u.password, u.phone, r.id as role_id, r.name as role FROM user as u" +
            " join role as r on u.role_id = r.id" +
            " where u.email = ?";
    public static final String SELECT_ALL_FROM_USER = "SELECT u.id, u.first_name, u.last_name, u.email," +
            " u.password, u.phone, r.id as role_id, r.name as role FROM user as u" +
            " join role as r on u.role_id = r.id";
    public static final String INSERT_INTO_USER_FIRST_NAME_EMAIL_PASSWORD_PHONE_ROLE_ID_VALUES = "insert into user (first_name, email, password, phone, role_id) values (?,?,?,?,?)";

}
