package com.epam.controller;


import com.epam.annotation.Timed;
import com.epam.entity.City;
import com.epam.entity.User;
import com.epam.service.interfaces.CityService;
import com.epam.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/adminPanel")
public class AdminListController {

    private final UserService userService;

    private final CityService cityService;

    @GetMapping
    public String showAllUsers(HttpSession session, Model model){
        String lang = session.getAttribute("lang").toString();

        List<User> users = userService.findAll();
        model.addAttribute("users", users);

        List<City> cities = cityService.findAll(lang);
        model.addAttribute("cities", cities);

        return "admin/list";
    }
}
