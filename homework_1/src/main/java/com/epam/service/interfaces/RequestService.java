package com.epam.service.interfaces;

import com.epam.dto.*;
import com.epam.entity.Request;
import com.epam.entity.User;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

public interface RequestService {

    RequestByUserDto createRequestByUser(Map<String, String> req, String lang, User user);

    List<Request> findAll(String lang);

    RequestDto findByIdAll(Long id, String lang);

    Request findById(Long id, String lang);

    List<RequestForUserProfileDto> findAllUserRequests(Long id, String lang);

    List<RequestForUserProfileDto> findAllUserRequests(String lang);

    Boolean updateByManager(String departure, String receiving, String statusId, String requestId);

    List<RequestForManagerDto> findAllForManager(String lang);

    Boolean updateToPaidStatus(RequestUpdateStatusDto requestUpdateStatusDto);

}
