package com.epam.service.interfaces;

import com.epam.dto.CargoDto;
import com.epam.entity.Cargo;
import org.springframework.stereotype.Service;

import java.util.List;


public interface CargoService {

    List<Cargo> findAll(String lang);

    Cargo create(CargoDto cargoDto);

    CargoDto findByRequestId(Long id, String lang);
}
