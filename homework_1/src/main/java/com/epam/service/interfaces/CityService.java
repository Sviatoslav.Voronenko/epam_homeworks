package com.epam.service.interfaces;

import com.epam.entity.City;

import java.util.List;

public interface CityService {
    City findById(Long id, String lang);

    List<City> findAll(String lang);

    City create(String name);

    void deleteById(Long id);
}
