package com.epam.service.impl;

import com.epam.annotation.Timed;
import com.epam.dao.interfaces.UserDAO;
import com.epam.entity.User;
import com.epam.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    @Override
    public User findById(Long id) {
        return userDAO.findById(id).orElseThrow(RuntimeException::new);
    }


    @Override
    public User findByEmail(String email) {
        return userDAO.findByEmail(email).orElse(null);
    }


    @Override
    public List<User> findAll() {
        return userDAO.findAll();
    }

    @Override
    public User create(Map<String, String> req) {
        User user = User.builder()
                .firstName(req.get("name"))
                .email(req.get("email"))
                .phone(req.get("phone"))
                .password(req.get("password"))
                .build();
        return userDAO.create(user);
    }
}
