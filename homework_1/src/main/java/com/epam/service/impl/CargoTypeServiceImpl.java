package com.epam.service.impl;

import com.epam.dao.impl.CargoTypeDAOImpl;
import com.epam.dao.interfaces.CargoTypeDAO;
import com.epam.entity.CargoType;
import com.epam.service.interfaces.CargoTypeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CargoTypeServiceImpl implements CargoTypeService {
    private final CargoTypeDAO cargoTypeDAO;

    @Override
    public CargoType findById(Long id, String lang) {
        return cargoTypeDAO.findById(id, lang).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<CargoType> findAll(String lang) {
        return cargoTypeDAO.findAll(lang);
    }

    @Override
    public CargoType create(String name) {
        CargoType cargoType = new CargoType();
        cargoType.setName(name);
        return cargoTypeDAO.create(cargoType);
    }

}
