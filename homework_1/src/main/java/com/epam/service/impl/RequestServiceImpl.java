package com.epam.service.impl;

import com.epam.dao.impl.RequestDAOImpl;
import com.epam.dao.interfaces.RequestDAO;
import com.epam.dto.*;
import com.epam.entity.City;
import com.epam.entity.Request;
import com.epam.entity.Status;
import com.epam.entity.Tariff;
import com.epam.entity.User;
import com.epam.service.interfaces.CargoService;
import com.epam.service.interfaces.CargoTypeService;
import com.epam.service.interfaces.CityService;
import com.epam.service.interfaces.RequestService;
import com.epam.service.interfaces.StatusService;
import com.epam.service.interfaces.TariffService;
import com.epam.util.TimeFormatter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
public class RequestServiceImpl implements RequestService {

    private final  RequestDAO requestDAO;

    private final  StatusService statusService;

    private final CityService cityService;

    private final  TariffService tariffService;

    private final  CargoTypeService cargoTypeService;

    private final RequestByUserDto requestByUserDto;

    @Override
    public RequestDto findByIdAll(Long id, String lang) {
        Request request = requestDAO.findById(id, lang).orElseThrow(RuntimeException::new);
        return RequestDto
                .builder()
                .id(request.getId())
                .address(request.getAddress())
                .cityFrom(request.getCityFrom())
                .cityTo(request.getCityTo())
                .cost(request.getCost())
                .dateOfCreation(request.getDateOfCreation())
                .dateOfPayment(request.getDateOfPayment())
                .departure(request.getDeparture())
                .receiving(request.getReceiving())
                .status(request.getStatus())
                .tariff(request.getTariff())
                .user(request.getUser())
                .build();
    }

    @Override
    public Request findById(Long id, String lang) {
        return requestDAO.findById(id, lang).orElse(null);
    }

    @Override
    public List<RequestForUserProfileDto> findAllUserRequests(Long id, String lang) {
        List<RequestForUserProfileDto> requestByUserDtos = new ArrayList<>();
        fillInList(requestByUserDtos, requestDAO.findAllUserRequests(id, lang));
        return requestByUserDtos;
    }

    @Override
    public List<RequestForUserProfileDto> findAllUserRequests(String lang) {
        List<RequestForUserProfileDto> requestByUserDtos = new ArrayList<>();
        fillInList(requestByUserDtos, requestDAO.findAllUserRequests(lang));
        return requestByUserDtos;
    }

    private void fillInList(List<RequestForUserProfileDto> requestByUserDtos, List<Request> allUserRequests) {
        allUserRequests.forEach(request -> requestByUserDtos.add(RequestForUserProfileDto.
                builder()
                .id(request.getId())
                .statusId(request.getStatus().getId())
                .cityFrom(request.getCityFrom().getName())
                .cityTo(request.getCityTo().getName())
                .departure(TimeFormatter.format(request.getDeparture()))
                .receiving(TimeFormatter.format(request.getReceiving()))
                .dateOfCreation(TimeFormatter.format(request.getDateOfCreation()))
                .dateOfPayment(TimeFormatter.format(request.getDateOfPayment()))
                .statusName(request.getStatus().getName())
                .cost(request.getCost())
                .address(request.getAddress())
                .build()));
    }

    @Override
    public List<RequestForManagerDto> findAllForManager(String lang) {
        List<RequestForManagerDto> requestForManagerDtos = new ArrayList<>();
        requestDAO.findAll(lang).forEach(request -> requestForManagerDtos.add(RequestForManagerDto
                .builder()
                .id(request.getId())
                .address(request.getAddress())
                .cityFromName(request.getCityFrom().getName())
                .cityToName(request.getCityTo().getName())
                .cost(request.getCost())
                .dateOfCreation(TimeFormatter.format(request.getDateOfCreation()))
                .departure(TimeFormatter.format(request.getDeparture()))
                .receiving(TimeFormatter.format(request.getReceiving()))
                .dateOfPayment(TimeFormatter.format(request.getDateOfPayment()))
                .statusName(request.getStatus().getName())
                .tariffName(request.getTariff().getName())
                .userFirstName(request.getUser().getFirstName())
                .userId(request.getUser().getId())
                .build()));
        return requestForManagerDtos;
    }

    @Override
    public Boolean updateByManager(String departure, String receiving, String statusId, String requestId) {
        return requestDAO.updateByManager(new RequestUpdatedByManagerDto(
                Long.parseLong(requestId),
                LocalDateTime.parse(departure),
                LocalDateTime.parse(receiving),
                Long.parseLong(statusId)
        ));
    }

    @Override
    public RequestByUserDto createRequestByUser(Map<String, String> req, String lang, User user) {
        requestByUserDto.setCargoDto(CargoDto
                .builder()
                .depth(Integer.parseInt(req.get("depth")))
                .width(Integer.parseInt(req.get("width")))
                .height(Integer.parseInt(req.get("height")))
                .weight(Integer.parseInt(req.get("weight")))
                .cargoType(cargoTypeService.findAll(lang).stream()
                        .filter(c -> c.getName().equals(req.get("cargoType")))
                        .findFirst().orElse(null))
                .build());

        double tariffCost;
        int cityPrice = 35;
        int countryPrice = 65;
        if (Long.parseLong(req.get("tariff")) == 1L) {
            tariffCost = cityPrice;
        } else {
            tariffCost = countryPrice;
        }

        CargoDto cargoDto = requestByUserDto.getCargoDto();
        BigDecimal volume = new BigDecimal(cargoDto.getDepth()).multiply(new BigDecimal(cargoDto.getHeight())).multiply(new BigDecimal(cargoDto.getWidth()));
        BigDecimal d = volume.multiply(new BigDecimal(0.1)).add(new BigDecimal(cargoDto.getWeight()).divide(new BigDecimal(20)));
        String cost = d.add(new BigDecimal(tariffCost)).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString();

        City cityFrom = cityService.findById(Long.parseLong(req.get("cityFrom")), lang);
        City cityTo = cityService.findById(Long.parseLong(req.get("cityTo")), lang);
        Tariff tariff = tariffService.findById(Long.parseLong(req.get("tariff")), lang);
        Status status = statusService.findById(1L, lang);

        Request request = Request
                .builder()
                .cityFrom(cityFrom)
                .cityTo(cityTo)
                .tariff(tariff)
                .address(req.get("address"))
                .cost(new BigDecimal(cost.replace(',', '.')))
                .user(user)
                .status(status)
                .dateOfCreation(LocalDateTime.now())
                .build();
        request = requestDAO.createRequestByUser(request);
        requestByUserDto.getCargoDto().setRequest(request);
        return requestByUserDto;
    }

    @Override
    public Boolean updateToPaidStatus(RequestUpdateStatusDto requestUpdateStatusDto) {
        return requestDAO.updateToPaidStatus(requestUpdateStatusDto);
    }

    @Override
    public List<Request> findAll(String lang) {
        return requestDAO.findAll(lang);
    }
}
