package com.epam.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/orderComplete")
public class OrderCompleteController {

    @GetMapping
    public String orderComplete(){
        return "orderComplete";
    }
}
