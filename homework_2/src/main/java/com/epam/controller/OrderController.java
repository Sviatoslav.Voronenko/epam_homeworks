package com.epam.controller;

import com.epam.dto.RequestByUserDto;
import com.epam.entity.CargoType;
import com.epam.entity.City;
import com.epam.entity.Tariff;
import com.epam.entity.User;
import com.epam.service.interfaces.CargoService;
import com.epam.service.interfaces.CargoTypeService;
import com.epam.service.interfaces.CityService;
import com.epam.service.interfaces.RequestService;
import com.epam.service.interfaces.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Controller
@RequestMapping("/order")
public class OrderController {

    private final CityService cityService;

    private final TariffService tariffService;

    private final RequestService requestService;

    private final CargoTypeService cargoTypeService;

    private final CargoService cargoService;

    private List<Tariff> tariffs;

    private List<City> cities;

    private List<CargoType> cargoTypes;


    @GetMapping
    public ModelAndView showOrderForm(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        String lang = session.getAttribute("lang").toString();

        cities = cityService.findAll(session.getAttribute("lang").toString());
        cargoTypes = cargoTypeService.findAll(lang);
        tariffs = tariffService.findAll(lang);

        modelAndView.addObject("cargotypes", cargoTypes);
        modelAndView.addObject("cities", cities);
        modelAndView.addObject("tariffs", tariffs);

        modelAndView.setViewName("order");
        return modelAndView;
    }


    @PostMapping
    public String createOrder(@RequestParam Map<String, String> req, HttpSession session) {
        RequestByUserDto requestByUserDto = requestService.createRequestByUser(req, session.getAttribute("lang").toString(), (User) session.getAttribute("user"));
        cargoService.create(requestByUserDto.getCargoDto());
        return "redirect:/thanks";
    }

    @GetMapping("orderComplete")
    public String showOrderComplete() {
        return "orderComplete";
    }
}
