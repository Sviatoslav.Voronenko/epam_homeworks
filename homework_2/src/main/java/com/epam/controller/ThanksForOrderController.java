package com.epam.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/thanks")
public class ThanksForOrderController {

    @GetMapping
    public String thanks(){
        return "thanksForOrder";
    }
}
