package com.epam.controller;


import com.epam.dto.CargoDto;
import com.epam.dto.RequestForUserProfileDto;
import com.epam.dto.RequestUpdateStatusDto;
import com.epam.entity.Status;
import com.epam.service.interfaces.CargoService;
import com.epam.service.interfaces.RequestService;
import com.epam.service.interfaces.StatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/request")
public class RequestController {

    private final CargoService cargoService;

    private final RequestService requestService;

    private final StatusService statusService;

    @GetMapping
    public String showAllUserRequests(Model model, HttpSession session){
        String lang = session.getAttribute("lang").toString();

        List<RequestForUserProfileDto> requestForUserProfileDtos = requestService.findAllUserRequests((Long) session.getAttribute("userId"), (String) session.getAttribute("lang"));
        model.addAttribute("requests", requestForUserProfileDtos);

        List<Status> statuses = statusService.findAll(lang);
        model.addAttribute("statuses", statuses);

        return "requests";
    }


    @GetMapping("{id}")
    public String showRequest(Model model, HttpSession session, @PathVariable Long id){
        String lang = session.getAttribute("lang").toString();

        CargoDto cargo = cargoService.findByRequestId(id, lang);

        List<RequestForUserProfileDto> requestForUserProfileDtos = requestService.findAllUserRequests((String) session.getAttribute("lang"));
        RequestForUserProfileDto requestForUserProfileDto = requestForUserProfileDtos.stream().filter(r -> r.getId() == id).findFirst().orElse(null);

        List<Status> statuses = statusService.findAll(lang);
        model.addAttribute("statuses", statuses);
        model.addAttribute("request", requestForUserProfileDto);
        model.addAttribute("cargo", cargo);
        return "request";
    }


    @PostMapping
    public String updateToPaidStatus(@RequestParam("request_id") Long requestId, Model model){
        requestService.updateToPaidStatus(new RequestUpdateStatusDto(requestId));
        return "redirect:/orderComplete";
    }
}