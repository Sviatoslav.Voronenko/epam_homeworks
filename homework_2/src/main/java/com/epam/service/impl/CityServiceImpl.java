package com.epam.service.impl;

import com.epam.dao.interfaces.CityDAO;
import com.epam.entity.City;
import com.epam.service.interfaces.CityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@RequiredArgsConstructor
@Service
public class CityServiceImpl implements CityService {
    private final CityDAO cityDAO;

    @Override
    public City findById(Long id, String lang) {
        return cityDAO.findById(id, lang).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<City> findAll(String lang) {
        return cityDAO.findAll(lang);
    }

    @Override
    public City create(String name) {
        City city = new City();
        city.setName(name);
        return cityDAO.create(city);
    }

    @Override
    public void deleteById(Long id) {
        cityDAO.deleteById(id);
    }
}
