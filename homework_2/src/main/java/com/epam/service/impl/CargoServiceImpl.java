package com.epam.service.impl;

import com.epam.dao.interfaces.CargoDAO;
import com.epam.dto.CargoDto;
import com.epam.entity.Cargo;
import com.epam.service.interfaces.CargoService;
import com.epam.service.interfaces.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CargoServiceImpl implements CargoService {
    private final CargoDAO cargoDAO;

    private final RequestService requestService;

    @Override
    public List<Cargo> findAll(String lang) {
        return cargoDAO.findAll(lang);
    }

    @Override
    public Cargo create(CargoDto cargoDto) {
        return cargoDAO.create(Cargo
                .builder()
                .weight(cargoDto.getWeight())
                .width(cargoDto.getWidth())
                .depth(cargoDto.getDepth())
                .height(cargoDto.getHeight())
                .cargoType(cargoDto.getCargoType())
                .requestId(cargoDto.getRequest())
                .build());
    }

    @Override
    public CargoDto findByRequestId(Long id, String lang) {
        Cargo byRequestId = cargoDAO.findByRequestId(id, lang).orElse(null);
        if (byRequestId == null) {
            return null;
        }
        return CargoDto
                .builder()
                .width(byRequestId.getWidth())
                .height(byRequestId.getHeight())
                .depth(byRequestId.getDepth())
                .weight(byRequestId.getWeight())
                .cargoType(byRequestId.getCargoType())
                .request(requestService.findById(byRequestId.getRequestId().getId(), lang))
                .build();
    }

    @Override
    public void deleteById(Long id) {

    }
}
