package com.epam.query;

public class RequestSqlQueries {
    public static final String SELECT_ALL_FROM_REQUEST = "SELECT req.*, cto.name as city_to_name, cfrom.name as city_from_name,u.first_name,\n" +
            "s.name as status_name,t.name as tariff_name\n" +
            "FROM request as req\n" +
            "join city as cto on req.city_id_to = cto.id\n" +
            "join city as cfrom on req.city_id_from = cfrom.id\n" +
            "join tariff as t on req.tariff_id = t.id\n" +
            "join user as u on req.user_id = u.id\n" +
            "join status as s on req.status_id = s.id";
    public static final String SELECT_ALL_FROM_REQUEST_TRANSLATE = "SELECT req.*, cto.name as city_to_name, cfrom.name as city_from_name,u.first_name,\n" +
            "s.name as status_name, t.name as tariff_name\n" +
            "FROM request as req\n" +
            "join city_has_languages as cto on req.city_id_to = cto.city_id\n" +
            "join city_has_languages as cfrom on req.city_id_from = cfrom.city_id\n" +
            "join tariff_has_languages as t on req.tariff_id = t.tariff_id\n" +
            "join user as u on req.user_id = u.id\n" +
            "join status_has_languages as s on req.status_id = s.status_id\n" +
            "join languages as lang on cto.languages_id = lang.id and cfrom.languages_id = lang.id\n" +
            "where lang.lang_code = ?\n" +
            "order by user_id";
    public static final String SELECT_FROM_REQUEST_WHERE_ID_TRANSLATE = "SELECT req.*, cto.name as city_to_name, cfrom.name as city_from_name,u.first_name,\n" +
            "s.name as status_name, t.name as tariff_name\n" +
            "FROM request as req\n" +
            "join city_has_languages as cto on req.city_id_to = cto.city_id\n" +
            "join city_has_languages as cfrom on req.city_id_from = cfrom.city_id\n" +
            "join tariff_has_languages as t on req.tariff_id = t.tariff_id\n" +
            "join user as u on req.user_id = u.id\n" +
            "join status_has_languages as s on req.status_id = s.status_id\n" +
            "join languages as lang on cto.languages_id = lang.id and cfrom.languages_id = lang.id\n" +
            "where req.id = ? and lang.lang_code = ?\n" +
            "order by user_id";
    public static final String SELECT_FROM_REQUEST_WHERE_ID = "SELECT req.*, cto.name as city_to_name, cfrom.name as city_from_name,u.first_name,\n" +
            "s.name as status_name,t.name as tariff_name\n" +
            "FROM request as req\n" +
            "join city as cto on req.city_id_to = cto.id\n" +
            "join city as cfrom on req.city_id_from = cfrom.id\n" +
            "join tariff as t on req.tariff_id = t.id\n" +
            "join user as u on req.user_id = u.id\n" +
            "join status as s on req.status_id = s.id\n" +
            "where req.id = ?";
    public static final String INSERT_INTO_REQUEST_CITY_ID_TO_CITY_ID_FROM_ADDRESS = "insert into request (city_id_to, city_id_from, address, status_id, date_of_creation, user_id, tariff_id, cost) values (?,?,?,?,?,?,?,?)";
    public static final String UPDATE_REQUEST_SET_DEPARTURE_STATUS_ID = "update request set departure = ?, receiving = ?, status_id = ?\n" +
            "where request.id = ?";
    public static final String FIND_ALL_USER_REQUESTS_TRANSLATE = "SELECT req.*, cto.name as city_to_name, cfrom.name as city_from_name,u.first_name,\n" +
            "s.name as status_name, t.name as tariff_name\n" +
            "FROM request as req\n" +
            "join city_has_languages as cto on req.city_id_to = cto.city_id\n" +
            "join city_has_languages as cfrom on req.city_id_from = cfrom.city_id\n" +
            "join tariff_has_languages as t on req.tariff_id = t.tariff_id\n" +
            "join user as u on req.user_id = u.id\n" +
            "join status_has_languages as s on req.status_id = s.status_id\n" +
            "join languages as lang on cto.languages_id = lang.id and cfrom.languages_id = lang.id\n" +
            "where u.id = ? and lang.lang_code = ?\n" +
            "order by user_id;";
    public static final String FIND_ALL_USER_REQUESTS = "SELECT req.*, cto.name as city_to_name, cfrom.name as city_from_name,u.first_name,s.name as status_name,t.name as tariff_name\n" +
            "FROM request as req\n" +
            "join city as cto on req.city_id_to = cto.id\n" +
            "join city as cfrom on req.city_id_from = cfrom.id\n" +
            "join tariff as t on req.tariff_id = t.id\n" +
            "join user as u on req.user_id = u.id\n" +
            "join status as s on req.status_id = s.id\n" +
            "where user_id = ?";
    public static final String UPDATE_REQUEST_SET_STATUS_ID_AND_DATE_OF_PAYMENT = "update request set status_id = 3, date_of_payment = now()\n" +
            "where id = ?;\n";
}
