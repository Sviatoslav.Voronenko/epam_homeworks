package com.epam.util;

import java.time.LocalDateTime;

public class TimeFormatter {
    public static String format(String str){
        return str.replace(" ", "T");
    }

    public static String format(LocalDateTime dateTime){
        return format(String.valueOf(dateTime));
    }
}
