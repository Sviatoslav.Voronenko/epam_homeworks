package com.epam.dao.impl;

import com.epam.dao.interfaces.RoleDAO;
import com.epam.entity.Role;
import com.epam.entity.RoleName;
import com.epam.exception.Messages;
import com.epam.mapper.ResultSetMapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.epam.query.RoleSqlQueries.DELETE_FROM_ROLE_WHERE_ID;
import static com.epam.query.RoleSqlQueries.INSERT_INTO_ROLE_NAME_VALUES;
import static com.epam.query.RoleSqlQueries.SELECT_FROM_ROLE;
import static com.epam.query.RoleSqlQueries.SELECT_FROM_ROLE_WHERE_ID;
import static com.epam.query.RoleSqlQueries.SELECT_FROM_ROLE_WHERE_NAME;

@Repository
public class RoleDAOImpl extends AbstractDAO<Role> implements RoleDAO {

    private static final Logger LOG = Logger.getLogger(RoleDAOImpl.class);

    private Map<RoleName, Role> cache = new EnumMap<RoleName, Role>(RoleName.class);

    public RoleDAOImpl(Connection connection, ResultSetMapper<Role> mapper) {
        super(connection, mapper);
    }

    @Override
    public Optional<Role> findById(Long id) {
        String sql = SELECT_FROM_ROLE_WHERE_ID;
        Role role = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                role = new Role(
                        resultSet.getLong("id"),
                        resultSet.getString("name"));
            }
            return Optional.ofNullable(role);
        } catch (SQLException e) {
            LOG.error(e);
            throw new RuntimeException(Messages.ERR_CANNOT_OBTAIN_USER_BY_ID, e);
        }

    }


    @Override
    public List<Role> findAll() {
        List<Role> roles = new ArrayList<>();
        String sql = SELECT_FROM_ROLE;
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                roles.add(new Role(
                        resultSet.getLong("id"),
                        resultSet.getString("name")));
            }
            return roles;
        } catch (SQLException e) {
            LOG.error(e);
            throw new RuntimeException(Messages.ERR_CANNOT_OBTAIN_USERS, e);
        }
    }

    @Override
    public Role create(Role entity) {
        String sql = INSERT_INTO_ROLE_NAME_VALUES;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.executeUpdate();
            LOG.info("Role successfully created");
            entity.setId(getLastInsertId());
            return entity;
        } catch (SQLException e) {
            LOG.error(e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public Role update(Role entity, Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {
        String sql = DELETE_FROM_ROLE_WHERE_ID;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            LOG.info("Role successfully deleted");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Role entity) {
        this.deleteById(entity.getId());
    }

    @Override
    public Optional<Role> findByName(String name) {
        String sql = SELECT_FROM_ROLE_WHERE_NAME;
        final Role role = cache.computeIfAbsent(RoleName.valueOf(name.toUpperCase()), roleName -> {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, name);
                ResultSet resultSet = preparedStatement.executeQuery();
                if (resultSet.next()) {
                    return new Role(
                            resultSet.getLong("id"),
                            resultSet.getString("name"));
                }
                return null;
            } catch (SQLException e) {
                LOG.error(e);
                throw new RuntimeException(e);
            }
        });
        return Optional.ofNullable(role);

    }
}
