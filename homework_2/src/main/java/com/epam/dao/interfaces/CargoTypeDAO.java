package com.epam.dao.interfaces;

import com.epam.entity.CargoType;

public interface CargoTypeDAO extends GenericDAOTranslator<CargoType, Long> {
}
