package com.epam.dao.interfaces;

import com.epam.entity.Role;

import java.util.Optional;

public interface RoleDAO extends GenericDAO<Role, Long> {
    Optional<Role> findByName(String name);
}
