package com.epam.dao.interfaces;

import com.epam.entity.Tariff;

public interface TariffDAO extends GenericDAOTranslator<Tariff, Long>{
}
