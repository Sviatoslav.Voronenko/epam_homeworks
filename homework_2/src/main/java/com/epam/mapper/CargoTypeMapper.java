package com.epam.mapper;

import com.epam.entity.CargoType;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CargoTypeMapper implements ResultSetMapper<CargoType> {
    @Override
    public List<CargoType> map(ResultSet resultSet) {
        List<CargoType> cargoTypes = new ArrayList<>();
        try {
            while (resultSet.next()) {
                CargoType cargoType = new CargoType(
                        resultSet.getLong("id"),
                        resultSet.getString("name")
                );
                cargoTypes.add(cargoType);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return cargoTypes;
    }
}
