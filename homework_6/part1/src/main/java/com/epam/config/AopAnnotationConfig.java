package com.epam.config;


import com.epam.service.NumberFinder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.epam")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AopAnnotationConfig {
    
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AopAnnotationConfig.class);
        context.refresh();
        NumberFinder numberFinder = context.getBean(NumberFinder.class);
        System.out.println(numberFinder.findNumber(100));
    }
}
