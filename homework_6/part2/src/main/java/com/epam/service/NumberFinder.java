package com.epam.service;

import com.epam.annotation.Logged;
import org.springframework.stereotype.Service;


public class NumberFinder {

    @Logged
    public int findNumber(int numb) {
        for (int i = 0; i < numb + 1; i++) {
            if (i == numb) {
                return i;
            }
        }
        return 0;
    }
}
