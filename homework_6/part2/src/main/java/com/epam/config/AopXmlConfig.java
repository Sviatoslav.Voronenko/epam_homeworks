package com.epam.config;

import com.epam.service.NumberFinder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AopXmlConfig {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("aop-config.xml");
        NumberFinder numberFinder = (NumberFinder) applicationContext.getBean("numberFinder");
        numberFinder.findNumber(100);
    }
}
