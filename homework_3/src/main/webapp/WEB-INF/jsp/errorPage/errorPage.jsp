<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

</head>
<body>
<h1><c:out value="${ex}"/></h1>
<h3><a href="/home"><fmt:message key="order_list_jsp.btn.back"/></a></h3>
</body>
</html>