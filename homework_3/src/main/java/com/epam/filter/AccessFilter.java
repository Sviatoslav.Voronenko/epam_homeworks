package com.epam.filter;

import com.epam.entity.RoleName;
import org.apache.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter({"/orderList", "/adminPanel", "/user"})
public class AccessFilter implements Filter {
    private Logger log = Logger.getLogger(AccessFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.debug("Filter initialization starts");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpServletResponse httpResponse = (HttpServletResponse) resp;
        String role = (String) httpRequest.getSession().getAttribute("userRole");
        if (role != null && role.equals(RoleName.ADMIN.getName())) {
            filterChain.doFilter(req, resp);
        } else {
            httpResponse.sendRedirect("/accessDenied");
            log.trace("access denied");
        }

    }

    @Override
    public void destroy() {
        log.debug("Filter destruction starts");


        log.debug("Filter destruction finished");
    }
}
