package com.epam.controller;

import com.epam.dto.RequestForManagerDto;
import com.epam.entity.Status;
import com.epam.exception.StatusesNotFoundException;
import com.epam.service.interfaces.RequestService;
import com.epam.service.interfaces.StatusService;
import com.epam.util.TimeFormatter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Controller
@RequestMapping("/orderList")
public class OrderListController {
    private final RequestService requestService;
    private final StatusService statusService;

    @GetMapping
    public ModelAndView showAllOrders(HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        String lang = session.getAttribute("lang").toString();
        List<RequestForManagerDto> requests = requestService.findAllForManager(lang);
        List<Status> statuses = statusService.findAll(lang);
        modelAndView.addObject("requests", requests);
        modelAndView.addObject("statuses", statuses);
        modelAndView.setViewName("admin/orderList");
        return modelAndView;
    }

    @PostMapping
    public String changeOrder(@RequestParam Map<String, String> req, HttpSession session, Model model) {
        String departure = req.get("departure_date_new");
        String receiving = req.get("receiving_date_new");
        String statusName = req.get("status");
        String requestId = req.get("requestId");
        String lang = session.getAttribute("lang").toString();
        if (departure.isEmpty()) {
            departure = TimeFormatter.format(req.get("oldDeparture"));
        }
        if (receiving.isEmpty()) {
            receiving = TimeFormatter.format(req.get("oldReceiving"));
        }
        List<Status> statuses = statusService.findAll(lang);
        String statusId = String.valueOf(statuses.stream().filter(s -> s.getName().equals(statusName)).findFirst().orElseThrow(StatusesNotFoundException::new).getId());
        requestService.updateByManager(departure, receiving, statusId, requestId);
        return "admin/orderList";
    }
}
