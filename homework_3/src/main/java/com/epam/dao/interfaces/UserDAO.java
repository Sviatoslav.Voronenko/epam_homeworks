package com.epam.dao.interfaces;

import com.epam.entity.User;

import java.util.Optional;

public interface UserDAO extends GenericDAO<User, Long>{
    Optional<User> findByEmail(String email);
}
