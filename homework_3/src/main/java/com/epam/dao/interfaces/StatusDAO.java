package com.epam.dao.interfaces;

import com.epam.entity.Status;

public interface StatusDAO extends GenericDAOTranslator<Status, Long> {
}
