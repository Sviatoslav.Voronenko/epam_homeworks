package com.epam.dao.impl;

import com.epam.entity.LangCode;
import com.epam.exception.DBException;
import com.epam.exception.MoreThanOneRecordException;
import com.epam.mapper.ResultSetMapper;
import lombok.RequiredArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
public class AbstractDAO<T> {

    public static final String SELECT_LAST_INSERT_ID_AS_ID = "select last_insert_id() as id";
    protected final Connection connection;
    protected final ResultSetMapper<T> mapper;


    protected Long getLastInsertId() {
        String sql = SELECT_LAST_INSERT_ID_AS_ID;
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            if (resultSet.next()) {
                return resultSet.getLong("id");
            }
            return null;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    protected Optional<T> findById(Long id,String lang ,String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            if (!lang.equals(LangCode.RU.getName())) preparedStatement.setString(2, lang);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() > 1) {
                throw new MoreThanOneRecordException("Received more than one record.");
            }
            return res.stream().findFirst();
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }


    protected Optional<T> findById(Long id, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() > 1) {
                throw new MoreThanOneRecordException("Received more than one record.");
            }
            return res.stream().findFirst();
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    protected List<T> findAllById(Long id, String lang, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            if (!lang.equals(LangCode.RU.getName())) preparedStatement.setString(2, lang);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() < 1) {
                return new ArrayList<>();
            }
            return res;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    protected Optional<T> findByName(String name, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> res = mapper.map(resultSet);
            if (res.size() > 1) {
                throw new MoreThanOneRecordException("Received more than one record.");
            }
            return res.stream().findFirst();
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    protected List<T> findAll(String lang, String sql) {
        if (!lang.equals(LangCode.RU.getName())) {
            return findAllTranslate(lang, sql);
        }
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            return mapper.map(resultSet);
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    protected List<T> findAll(String sql) {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            return mapper.map(resultSet);
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    protected List<T> findAllTranslate(String lang, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, lang);
            ResultSet resultSet = preparedStatement.executeQuery();
            return mapper.map(resultSet);
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    protected Long create(String name, String sql) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
            return getLastInsertId();
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }
}
