package com.epam.dao.impl;

import com.epam.dao.interfaces.RequestDAO;
import com.epam.dto.RequestUpdateStatusDto;
import com.epam.dto.RequestUpdatedByManagerDto;
import com.epam.entity.LangCode;
import com.epam.entity.Request;
import com.epam.exception.DBException;
import com.epam.mapper.ResultSetMapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

import static com.epam.query.RequestSqlQueries.FIND_ALL_USER_REQUESTS;
import static com.epam.query.RequestSqlQueries.FIND_ALL_USER_REQUESTS_TRANSLATE;
import static com.epam.query.RequestSqlQueries.INSERT_INTO_REQUEST_CITY_ID_TO_CITY_ID_FROM_ADDRESS;
import static com.epam.query.RequestSqlQueries.SELECT_ALL_FROM_REQUEST;
import static com.epam.query.RequestSqlQueries.SELECT_ALL_FROM_REQUEST_TRANSLATE;
import static com.epam.query.RequestSqlQueries.SELECT_FROM_REQUEST_WHERE_ID;
import static com.epam.query.RequestSqlQueries.SELECT_FROM_REQUEST_WHERE_ID_TRANSLATE;
import static com.epam.query.RequestSqlQueries.UPDATE_REQUEST_SET_DEPARTURE_STATUS_ID;
import static com.epam.query.RequestSqlQueries.UPDATE_REQUEST_SET_STATUS_ID_AND_DATE_OF_PAYMENT;

@Repository
public class RequestDAOImpl extends AbstractDAO<Request> implements RequestDAO {

    private static final Logger LOG = Logger.getLogger(RequestDAOImpl.class);

    public RequestDAOImpl(Connection connection, ResultSetMapper<Request> mapper) {
        super(connection, mapper);
    }


    @Override
    public Optional<Request> findById(Long id, String lang) {
        String sql;
        if (!LangCode.RU.getName().equals(lang)) {
            sql = SELECT_FROM_REQUEST_WHERE_ID_TRANSLATE;
        } else {
            sql = SELECT_FROM_REQUEST_WHERE_ID;
        }
        return super.findById(id, lang, sql);
    }

    @Override
    public List<Request> findAll(String lang) {
        String sql;
        if (!LangCode.RU.getName().equals(lang)) {
            sql = SELECT_ALL_FROM_REQUEST_TRANSLATE;
            return super.findAllTranslate(lang, sql);
        }
        sql = SELECT_ALL_FROM_REQUEST;
        return super.findAll(lang, sql);
    }

    @Override
    public List<Request> findAllUserRequests(Long id, String lang) {
        String sql;
        if (!LangCode.RU.getName().equals(lang)) {
            sql = FIND_ALL_USER_REQUESTS_TRANSLATE;
        } else {
            sql = FIND_ALL_USER_REQUESTS;
        }
        return super.findAllById(id, lang, sql);
    }

    @Override
    public List<Request> findAllUserRequests(String lang) {
        String sql;
        if (!LangCode.RU.getName().equals(lang)) {
            sql = SELECT_ALL_FROM_REQUEST_TRANSLATE;
        } else {
            sql = SELECT_ALL_FROM_REQUEST;
        }
        return super.findAll(lang, sql);
    }

    @Override
    public List<Request> findAllTranslate(String lang) {
        return null;
    }

    @Override
    public Request create(Request entity) {
        return null;
    }

    @Override
    public Request createTranslate(Request entity, String lang) {
        return null;
    }

    @Override
    public Request createRequestByUser(Request request) {
        String sql = INSERT_INTO_REQUEST_CITY_ID_TO_CITY_ID_FROM_ADDRESS;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, request.getCityTo().getId());
            preparedStatement.setLong(2, request.getCityFrom().getId());
            preparedStatement.setString(3, request.getAddress());
            preparedStatement.setLong(4, request.getStatus().getId());
            preparedStatement.setTimestamp(5, new Timestamp(request.getDateOfCreation().toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()));
            preparedStatement.setLong(6, request.getUser().getId());
            preparedStatement.setLong(7, request.getTariff().getId());
            preparedStatement.setBigDecimal(8, request.getCost());
            preparedStatement.executeUpdate();
            request.setId(getLastInsertId());
            return request;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    public Boolean updateByManager(RequestUpdatedByManagerDto requestUpdatedByManagerDto) {
        String sql = UPDATE_REQUEST_SET_DEPARTURE_STATUS_ID;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setTimestamp(1, new Timestamp(requestUpdatedByManagerDto.getDeparture().toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()));
            preparedStatement.setTimestamp(2, new Timestamp(requestUpdatedByManagerDto.getReceiving().toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()));
            preparedStatement.setLong(3, requestUpdatedByManagerDto.getStatusId());
            preparedStatement.setLong(4, requestUpdatedByManagerDto.getId());
            preparedStatement.executeUpdate();
            LOG.info("Request updated id -> " + requestUpdatedByManagerDto.getId());
            return true;
        } catch (SQLException e) {
            LOG.error(e);
            throw new DBException(e);
        }
    }

    @Override
    public Request update(Request entity, Long aLong, String lang) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public Boolean updateToPaidStatus(RequestUpdateStatusDto updateStatus) {
        String sql = UPDATE_REQUEST_SET_STATUS_ID_AND_DATE_OF_PAYMENT;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, updateStatus.getRequestId());
            preparedStatement.executeUpdate();
            LOG.info("Status and date of payment updated where request id -> " + updateStatus.getRequestId());
            return true;
        } catch (SQLException e) {
            LOG.error("Cannot update a request", e);
            throw new DBException(e);
        }
    }

    @Override
    public void delete(Request entity) {

    }
}
