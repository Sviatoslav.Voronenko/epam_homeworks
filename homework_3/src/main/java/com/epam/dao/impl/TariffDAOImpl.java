package com.epam.dao.impl;

import com.epam.dao.interfaces.TariffDAO;
import com.epam.entity.LangCode;
import com.epam.entity.Tariff;
import com.epam.mapper.ResultSetMapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static com.epam.query.TariffSqlQueries.INSERT_INTO_TARIFF;
import static com.epam.query.TariffSqlQueries.SELECT_FROM_TARIFF;
import static com.epam.query.TariffSqlQueries.SELECT_FROM_TARIFFS_TRANSLATE;
import static com.epam.query.TariffSqlQueries.SELECT_FROM_TARIFF_TRANSLATE;
import static com.epam.query.TariffSqlQueries.SELECT_FROM_TARIFF_WHERE_ID;

@Repository
public class TariffDAOImpl extends AbstractDAO<Tariff> implements TariffDAO {
    private static final Logger LOG = Logger.getLogger(TariffDAOImpl.class);

    public TariffDAOImpl(Connection connection, ResultSetMapper<Tariff> mapper) {
        super(connection, mapper);
    }


    @Override
    public Optional<Tariff> findById(Long id, String lang) {
        String sql;
        if (lang.equals(LangCode.RU.getName())) {
            sql = SELECT_FROM_TARIFF_WHERE_ID;
        } else {
            sql = SELECT_FROM_TARIFF_TRANSLATE;
        }
        return super.findById(id, lang, sql);
    }

    @Override
    public List<Tariff> findAll(String lang) {
        if (!lang.equals(LangCode.RU.getName())) {
            return findAllTranslate(lang);
        }
        String sql = SELECT_FROM_TARIFF;
        return super.findAll(lang, sql);
    }

    @Override
    public List<Tariff> findAllTranslate(String lang) {
        String sql = SELECT_FROM_TARIFFS_TRANSLATE;
        return super.findAllTranslate(lang, sql);
    }

    @Override
    public Tariff create(Tariff entity) {
        String sql = INSERT_INTO_TARIFF;
        entity.setId(super.create(entity.getName(), sql));
        return entity;
    }

    @Override
    public Tariff createTranslate(Tariff entity, String lang) {
        return null;
    }

    @Override
    public Tariff update(Tariff entity, Long id, String lang) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void delete(Tariff entity) {

    }
}
