package com.epam.dto;

import com.epam.entity.City;
import com.epam.entity.Tariff;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestInfoByUserDto {
    private String cityFrom;
    private String cityTo;
    private String cost;
    private City city;
    private Tariff tariff;
}
