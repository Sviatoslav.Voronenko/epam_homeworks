package com.epam.mapper;

import com.epam.entity.Status;
import com.epam.exception.DBException;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class StatusMapper implements ResultSetMapper<Status> {
    @Override
    public List<Status> map(ResultSet resultSet) {
        List<Status> statuses = new ArrayList<>();
        try {
            while (resultSet.next()){
                Status status= new Status(
                        resultSet.getLong("id"),
                        resultSet.getString("name")
                );
                statuses.add(status);
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return statuses;
    }
}
