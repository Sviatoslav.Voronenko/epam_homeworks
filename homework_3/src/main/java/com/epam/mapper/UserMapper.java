package com.epam.mapper;

import com.epam.entity.Role;
import com.epam.entity.User;
import com.epam.exception.DBException;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper implements ResultSetMapper<User> {
    @Override
    public List<User> map(ResultSet resultSet) {
        List<User> users = new ArrayList<>();
        try {
            while (resultSet.next()) {
                users.add(new User(
                        resultSet.getLong("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("phone"),
                        new Role(resultSet.getLong("role_id"),
                                resultSet.getString("role"))));
            }
            return users;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }
}
