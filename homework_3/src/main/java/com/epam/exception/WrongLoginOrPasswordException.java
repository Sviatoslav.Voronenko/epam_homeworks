package com.epam.exception;

import java.io.IOException;

public class WrongLoginOrPasswordException extends RuntimeException {

    public WrongLoginOrPasswordException(String message) {
        super(message);
    }
}
