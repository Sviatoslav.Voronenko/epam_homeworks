package com.epam.exception;

import java.io.IOException;

public class EmptyLoginOrPasswordException extends RuntimeException {

    public EmptyLoginOrPasswordException(String message) {
        super(message);
    }
}
