package com.epam.exception;

public class MoreThanOneRecordException extends RuntimeException {
    public MoreThanOneRecordException(String message) {
        super(message);
    }
}
