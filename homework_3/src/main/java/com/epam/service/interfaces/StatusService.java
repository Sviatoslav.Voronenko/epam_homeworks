package com.epam.service.interfaces;

import com.epam.entity.Status;

import java.util.List;

public interface StatusService {
    Status findById(Long id, String lang);

    List<Status> findAll(String lang);

    Status create(String name);

}
