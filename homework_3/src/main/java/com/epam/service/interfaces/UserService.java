package com.epam.service.interfaces;

import com.epam.entity.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    User findById(Long id);

    User findByEmail(String email);

    List<User> findAll();

    User create(Map<String, String> req);
}
