package com.epam.service.impl;

import com.epam.dao.interfaces.TariffDAO;
import com.epam.entity.Tariff;
import com.epam.exception.TariffNotFoundException;
import com.epam.service.interfaces.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@RequiredArgsConstructor
@Service
public class TariffServiceImpl implements TariffService {

    private final TariffDAO tariffDAO;

    @Override
    public Tariff findById(Long id, String lang) {
        return tariffDAO.findById(id, lang).orElseThrow(TariffNotFoundException::new);
    }

    @Override
    public List<Tariff> findAll(String lang) {
        return tariffDAO.findAll(lang);
    }

    @Override
    public Tariff create(String name) {
        return null;
    }

}
