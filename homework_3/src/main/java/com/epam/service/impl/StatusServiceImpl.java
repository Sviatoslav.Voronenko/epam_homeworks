package com.epam.service.impl;

import com.epam.dao.interfaces.StatusDAO;
import com.epam.entity.Status;
import com.epam.exception.StatusesNotFoundException;
import com.epam.service.interfaces.StatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class StatusServiceImpl implements StatusService {

    private final StatusDAO statusDAO;

    @Override
    public Status findById(Long id, String lang) {
        return statusDAO.findById(id, lang).orElseThrow(StatusesNotFoundException::new);
    }

    @Override
    public List<Status> findAll(String lang) {
        return statusDAO.findAll(lang);
    }

    @Override
    public Status create(String name) {
        Status status = new Status();
        status.setName(name);
        return statusDAO.create(status);
    }

}
