package com.epam.service.impl;

import com.epam.dao.interfaces.RoleDAO;
import com.epam.entity.Role;
import com.epam.exception.RoleNotFoundException;
import com.epam.service.interfaces.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleDAO roleDAO;

    @Override
    public Role findById(Long id) {
        return roleDAO.findById(id).orElseThrow(RoleNotFoundException::new);
    }

    @Override
    public List<Role> findAll() {
        return roleDAO.findAll();
    }

    @Override
    public Role create(String name) {
        Role role = new Role();
        role.setName(name);
        return roleDAO.create(role);
    }

    @Override
    public Role findByName(String name) {
        return roleDAO.findByName(name).orElseThrow(RoleNotFoundException::new);
    }

    @Override
    public Role update(Role role, Long id) {
        return null;
    }


    @Override
    public void deleteById(Long id) {
        roleDAO.deleteById(id);
    }

    @Override
    public void delete(String name) {
        Role role = new Role();
        role.setName(name);
        roleDAO.delete(role);
    }
}
