package com.epam.service;

import com.epam.entity.User;
import com.epam.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = repository.findByLogin(s).orElse(null);
        if(user == null){
            throw new UsernameNotFoundException("User " + s + " not found");
        }



        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        String role = user.getRole();

        GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + role);
        grantedAuthorities.add(authority);

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                grantedAuthorities
        );

        return userDetails;
    }
}
