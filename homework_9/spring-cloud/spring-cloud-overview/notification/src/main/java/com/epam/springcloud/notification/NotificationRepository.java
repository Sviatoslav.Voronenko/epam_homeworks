package com.epam.springcloud.notification;

import java.util.ArrayList;
import java.util.List;

public class NotificationRepository {

    List<Notification> notifications = new ArrayList<>();

    public Notification add(String userName, Notification.Notifier notifier){
        Notification notification = new Notification();
        notification.setUser(userName);
        notification.setNotifyBy(notifier);
        notifications.add(notification);
        return notification;


    }

    public List<Notification> getAll(){
        return notifications;
    }
}
