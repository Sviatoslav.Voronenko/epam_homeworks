package com.epam.exception;

public class DBException extends RuntimeException {
    public DBException(String reason) {
        super(reason);
    }


    public DBException(Throwable cause) {
        super(cause);
    }

    public DBException() {
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }
}
