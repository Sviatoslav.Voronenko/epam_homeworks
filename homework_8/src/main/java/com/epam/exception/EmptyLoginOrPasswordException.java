package com.epam.exception;

public class EmptyLoginOrPasswordException extends RuntimeException {

    public EmptyLoginOrPasswordException(String message) {
        super(message);
    }
}
