package com.epam.query;

public class TariffSqlQueries {
    public static final String SELECT_FROM_TARIFF_TRANSLATE = "SELECT tl.tariff_id as id, tl.name as name FROM tariff_has_languages as tl" +
            " join languages as lang on tl.languages_id = lang.id" +
            " where tl.tariff_id = ? and " +
            " lang.lang_code = ?";
    public static final String SELECT_FROM_TARIFF_WHERE_ID = "select * from tariff where id = ?";
    public static final String SELECT_FROM_TARIFF = "select * from tariff";
    public static final String SELECT_FROM_TARIFFS_TRANSLATE = "SELECT tl.tariff_id as id, tl.name as name FROM tariff_has_languages as tl" +
            " join languages as lang on tl.languages_id = lang.id" +
            " where lang.lang_code = ?";
    public static final String INSERT_INTO_TARIFF = "insert into tariff (name) values (?)";

}
