package com.epam.query;

public class StatusSqlQueries {
    public static final String SELECT_FROM_STATUS_WHERE_ID = "select * from status where id = ?";
    public static final String SELECT_FROM_STATUS = "select * from status";
    public static final String SELECT_FROM_STATUS_TRANSLATE = "SELECT sl.status_id as id, sl.name as name FROM delivery.status_has_languages as sl" +
            " join languages as lang on sl.languages_id = lang.id" +
            " where sl.status_id = ?  and" +
            " lang.lang_code = ?";
    public static final String SELECT_ALL_FROM_STATUSES_TRANSLATE = "SELECT sl.status_id as id, sl.name as name FROM delivery.status_has_languages as sl" +
            " join languages as lang on sl.languages_id = lang.id" +
            " where lang.lang_code = ?";
    public static final String INSERT_INTO_STATUS_NAME_VALUES = "insert into status (name) values (?)";
    public static final String DELETE_FROM_STATUS_WHERE_ID = "delete from status where id = ?";

}
