package com.epam.controller;


import com.epam.entity.User;
import com.epam.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @GetMapping("{id}")
    public String showUserInfo(@PathVariable Long id, Model model){
        User user = userService.findById(id);
        model.addAttribute("userProfile", user);
        return "admin/user";
    }
}
