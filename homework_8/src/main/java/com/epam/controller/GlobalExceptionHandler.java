package com.epam.controller;

import com.epam.exception.EmptyLoginOrPasswordException;
import com.epam.exception.EntityNotFoundException;
import com.epam.exception.MoreThanOneRecordException;
import com.epam.exception.WrongLoginOrPasswordException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;

import static com.epam.exception.Messages.ENTITY_NOT_FOUND;
import static com.epam.exception.Messages.ERROR_PAGE;
import static com.epam.exception.Messages.EX;
import static com.epam.exception.Messages.PAGE_NOT_FOUND_404;
import static com.epam.exception.Messages.SOMETHING_WENT_WRONG;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = Logger.getLogger(GlobalExceptionHandler.class);


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleException(HttpServletRequest request, NoHandlerFoundException e) {
        logger.error(e);
        request.setAttribute(EX, SOMETHING_WENT_WRONG);
        return ERROR_PAGE;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String handle404Exception(HttpServletRequest request, NoHandlerFoundException e) {
        logger.error(e);
        request.setAttribute(EX, PAGE_NOT_FOUND_404);
        return ERROR_PAGE;
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public String handleEntityNotFoundException(HttpServletRequest request, Exception e) {
        logger.error(e.getMessage(), e);
        request.setAttribute(EX, ENTITY_NOT_FOUND);
        return ERROR_PAGE;
    }

    @ExceptionHandler(EmptyLoginOrPasswordException.class)
    public String handleEmptyLoginOrPasswordException(HttpServletRequest request, Exception e) {
        logger.error(e.getMessage(), e);
        request.setAttribute(EX, e.getMessage());
        return ERROR_PAGE;
    }

    @ExceptionHandler(WrongLoginOrPasswordException.class)
    public String handleWrongLoginOrPasswordException(HttpServletRequest request, Exception e) {
        logger.error(e.getMessage(), e);
        request.setAttribute(EX, e.getMessage());
        return ERROR_PAGE;
    }

    @ExceptionHandler(MoreThanOneRecordException.class)
    public String handleMoreThanOneRecordException(HttpServletRequest request, Exception e) {
        logger.error(e.getMessage(), e);
        request.setAttribute(EX, e.getMessage());
        return ERROR_PAGE;
    }
}
