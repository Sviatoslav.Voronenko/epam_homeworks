package com.epam.controller;


import com.epam.entity.RoleName;
import com.epam.entity.User;
import com.epam.exception.EmptyLoginOrPasswordException;
import com.epam.exception.WrongLoginOrPasswordException;
import com.epam.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RequiredArgsConstructor
@Controller
@RequestMapping("/login")
public class LoginController {

    private static final Logger LOG = Logger.getLogger(LoginController.class);

    private final UserService userService;

    @GetMapping
    public String printLoginPage() {
        return "login";
    }

    @PostMapping
    public String loginUser(@RequestParam Map<String, String> reqParams, HttpSession session) throws Exception {
        String email = reqParams.get("name");
        LOG.trace("Request parameter: login --> " + email);

        String password = reqParams.get("password");

        if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
            throw new EmptyLoginOrPasswordException("Login/password cannot be empty");
        }

        User user = userService.findByEmail(email);
        LOG.trace("Found in DB: user --> " + user);


        if (user != null && user.getEmail().equals(email) && user.getPassword().equals(password)) {
            String userRole = user.getRole().getName();
            String lang = session.getAttribute("lang").toString();
            session.setAttribute("login", email);
            session.setAttribute("firstName", user.getFirstName());
            session.setAttribute("user", user);
            session.setAttribute("userId", user.getId());

            LOG.trace("Set the session attribute: user --> " + user);
            session.setAttribute("userRole", userRole);

            LOG.trace("Set the session attribute: userRole --> " + userRole);
            session.setAttribute("lang", lang);

            session.setMaxInactiveInterval(30 * 60);
            LOG.info("User " + user + " logged as " + userRole.toLowerCase());
            if (user.getRole().getName().equals(RoleName.ADMIN.getName())) {
                session.setAttribute("isAdmin", RoleName.ADMIN.getName());
                LOG.trace("Set the session attribute: isAdmin --> true");
            }

            LOG.debug("LoginServlet finished");

            return "redirect:/home";
        } else {
            throw new WrongLoginOrPasswordException("Wrong Login/password");
        }
    }
}
