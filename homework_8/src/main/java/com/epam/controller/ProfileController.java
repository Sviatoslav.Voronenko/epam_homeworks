package com.epam.controller;

import com.epam.dto.UserInfoForUserDto;
import com.epam.entity.User;
import com.epam.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@RequiredArgsConstructor
@Controller
@RequestMapping("/profile")
public class ProfileController {

    private final UserService userService;

    @GetMapping
    public String showUserinfo(HttpSession session, Model model) {
        User user = userService.findByEmail(String.valueOf(session.getAttribute("login")));
        UserInfoForUserDto userInfoForUserDto = UserInfoForUserDto
                .builder()
                .first_name(user.getFirstName())
                .last_name(user.getLastName())
                .email(user.getEmail())
                .phone(user.getPhone())
                .build();
        model.addAttribute("user", userInfoForUserDto);
        return "profile";
    }
}
