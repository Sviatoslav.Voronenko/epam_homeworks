package com.epam.controller;


import com.epam.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RequiredArgsConstructor
@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private final UserService userService;

    @GetMapping
    public String showRegistration(){
        return "registration";
    }

    @PostMapping
    public void register(@RequestParam Map<String, String> req, HttpServletResponse resp){
        String firstName = req.get("name");
        String email = req.get("email");
        String phone = req.get("phone");
        String password = req.get("password");
        if (userService.findByEmail(email) != null) {
            resp.setStatus(1100);
            return;
        }
        if (firstName != null && email != null && phone != null && password != null) {
            userService.create(req);
        }
    }
}
