package com.epam.dao.impl;

import com.epam.dao.interfaces.CargoDAO;
import com.epam.entity.Cargo;
import com.epam.entity.LangCode;
import com.epam.exception.DBException;
import com.epam.mapper.ResultSetMapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static com.epam.query.CargoSqlQueries.INSERT_INTO_CARGO_CARGOTYPE_ID_WEIGHT_HEIGHT_WIDTH_DEPTH_REQUEST_ID_VALUES;
import static com.epam.query.CargoSqlQueries.SELECT_ALL_FROM_CARGO;
import static com.epam.query.CargoSqlQueries.SELECT_ALL_FROM_CARGO_TRANSLATE;
import static com.epam.query.CargoSqlQueries.SELECT_FROM_CARGO_BY_REQUEST_ID;
import static com.epam.query.CargoSqlQueries.SELECT_FROM_CARGO_BY_REQUEST_ID_TRANSLATE;
import static com.epam.query.CargoSqlQueries.SELECT_FROM_CARGO_TRANSLATE_WHERE_ID;
import static com.epam.query.CargoSqlQueries.SELECT_FROM_CARGO_WHERE_ID;


@Repository
public class CargoDAOImpl extends AbstractDAO<Cargo> implements CargoDAO {

    private static final Logger LOG = Logger.getLogger(CargoDAOImpl.class);

    public CargoDAOImpl(Connection connection, ResultSetMapper<Cargo> mapper) {
        super(connection, mapper);
    }

    @Override
    public Optional<Cargo> findById(Long id, String lang) {
        String sql;
        if (!LangCode.RU.getName().equals(lang)) {
            sql = SELECT_FROM_CARGO_WHERE_ID;
        } else {
            sql = SELECT_FROM_CARGO_TRANSLATE_WHERE_ID;
        }
        return super.findById(id, lang, sql);
    }

    @Override
    public List<Cargo> findAll(String lang) {
        String sql;
        if (!LangCode.RU.getName().equals(lang)) {
            sql = SELECT_ALL_FROM_CARGO_TRANSLATE;
            return super.findAll(lang, sql);
        }
        sql = SELECT_ALL_FROM_CARGO;
        return super.findAll(lang, sql);
    }

    @Override
    public Optional<Cargo> findByRequestId(Long id, String lang) {
        String sql;
        if (!LangCode.RU.getName().equals(lang)) {
            sql = SELECT_FROM_CARGO_BY_REQUEST_ID_TRANSLATE;
            return super.findById(id, lang, sql);
        }
        sql = SELECT_FROM_CARGO_BY_REQUEST_ID;
        return super.findById(id, lang, sql);
    }

    @Override
    public List<Cargo> findAllTranslate(String lang) {
        return null;
    }

    @Override
    public Cargo create(Cargo entity) {
        String sql = INSERT_INTO_CARGO_CARGOTYPE_ID_WEIGHT_HEIGHT_WIDTH_DEPTH_REQUEST_ID_VALUES;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getCargoType().getId());
            preparedStatement.setInt(2, entity.getWeight());
            preparedStatement.setInt(3, entity.getHeight());
            preparedStatement.setInt(4, entity.getWidth());
            preparedStatement.setInt(5, entity.getDepth());
            preparedStatement.setLong(6, entity.getRequestId().getId());
            preparedStatement.executeUpdate();
            entity.setId(getLastInsertId());
            return entity;
        } catch (SQLException e) {
            throw new DBException(e);
        }
    }

    @Override
    public Cargo createTranslate(Cargo entity, String lang) {
        return null;
    }

    @Override
    public Cargo update(Cargo entity, Long id, String lang) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Cargo entity) {

    }


}
