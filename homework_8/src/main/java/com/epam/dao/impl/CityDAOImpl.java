package com.epam.dao.impl;

import com.epam.dao.interfaces.CityDAO;
import com.epam.entity.City;
import com.epam.entity.LangCode;
import com.epam.exception.DBException;
import com.epam.mapper.ResultSetMapper;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static com.epam.query.CitySqlQueries.DELETE_FROM_CITY_WHERE_ID;
import static com.epam.query.CitySqlQueries.INSERT_INTO_CITY_NAME_VALUES;
import static com.epam.query.CitySqlQueries.SELECT_ALL_FROM_CITIES_TRANSLATE;
import static com.epam.query.CitySqlQueries.SELECT_FROM_CITY;
import static com.epam.query.CitySqlQueries.SELECT_FROM_CITY_TRANSLATE;
import static com.epam.query.CitySqlQueries.SELECT_FROM_CITY_WHERE_ID;

@Repository
public class CityDAOImpl extends AbstractDAO<City> implements CityDAO {

    private static final Logger LOG = Logger.getLogger(CityDAOImpl.class);

    public CityDAOImpl(Connection connection, ResultSetMapper<City> mapper) {
        super(connection, mapper);
    }


    @Override
    public Optional<City> findById(Long id, String lang) {
        String sql;
        if (lang.equals(LangCode.RU.getName())) {
            sql = SELECT_FROM_CITY_WHERE_ID;
        } else {
            sql = SELECT_FROM_CITY_TRANSLATE;
        }
        return super.findById(id, lang, sql);
    }

    @Override
    public List<City> findAll(String lang) {
        if (!lang.equals(LangCode.RU.getName())) {
            return findAllTranslate(lang);
        }
        String sql = SELECT_FROM_CITY;
        return super.findAll(lang, sql);
    }

    @Override
    public List<City> findAllTranslate(String lang) {
        String sql = SELECT_ALL_FROM_CITIES_TRANSLATE;
        return super.findAllTranslate(lang, sql);
    }


    @Override
    public City create(City entity) {
        String sql = INSERT_INTO_CITY_NAME_VALUES;
        entity.setId(super.create(entity.getName(), sql));
        return entity;
    }

    @Override
    public City createTranslate(City entity, String lang) {
        return null;
    }

    @Override
    public City update(City entity, Long aLong, String lang) {
        return null;
    }


    @Override
    public void deleteById(Long id) {
        String sql = DELETE_FROM_CITY_WHERE_ID;
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.executeUpdate();
            LOG.info("del");
        } catch (SQLException e) {
            LOG.error(e);
            throw new DBException(e);
        }
    }

    @Override
    public void delete(City entity) {
        deleteById(entity.getId());
    }
}
