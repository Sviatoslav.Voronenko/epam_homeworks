package com.epam.dao.interfaces;

import com.epam.entity.City;

public interface CityDAO extends GenericDAOTranslator<City, Long> {
}
