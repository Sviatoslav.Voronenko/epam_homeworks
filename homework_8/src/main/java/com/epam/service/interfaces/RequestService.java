package com.epam.service.interfaces;

import com.epam.dto.RequestByUserDto;
import com.epam.dto.RequestDto;
import com.epam.dto.RequestForManagerDto;
import com.epam.dto.RequestForUserProfileDto;
import com.epam.dto.RequestUpdateStatusDto;
import com.epam.entity.Request;
import com.epam.entity.User;

import java.util.List;
import java.util.Map;

public interface RequestService {

    RequestByUserDto createRequestByUser(Map<String, String> req, String lang, User user);

    List<Request> findAll(String lang);

    RequestDto findByIdAll(Long id, String lang);

    Request findById(Long id, String lang);

    List<RequestForUserProfileDto> findAllUserRequests(Long id, String lang);

    List<RequestForUserProfileDto> findAllUserRequests(String lang);

    Boolean updateByManager(String departure, String receiving, String statusId, String requestId);

    List<RequestForManagerDto> findAllForManager(String lang);

    Boolean updateToPaidStatus(RequestUpdateStatusDto requestUpdateStatusDto);

}
