package com.epam.service.interfaces;

import com.epam.entity.Tariff;

import java.util.List;

public interface TariffService {
    Tariff findById(Long id, String lang);

    List<Tariff> findAll(String lang);

    Tariff create(String name);

}
