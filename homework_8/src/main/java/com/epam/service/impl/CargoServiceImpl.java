package com.epam.service.impl;

import com.epam.dao.interfaces.CargoDAO;
import com.epam.dto.CargoDto;
import com.epam.entity.Cargo;
import com.epam.service.interfaces.CargoService;
import com.epam.service.interfaces.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class CargoServiceImpl implements CargoService {
    private final CargoDAO cargoDAO;

    private final RequestService requestService;

    @Override
    public List<Cargo> findAll(String lang) {
        return cargoDAO.findAll(lang);
    }

    @Override
    public Cargo create(CargoDto cargoDto) {
        return cargoDAO.create(Cargo
                .builder()
                .weight(cargoDto.getWeight())
                .width(cargoDto.getWidth())
                .depth(cargoDto.getDepth())
                .height(cargoDto.getHeight())
                .cargoType(cargoDto.getCargoType())
                .requestId(cargoDto.getRequest())
                .build());
    }

    @Override
    public CargoDto findByRequestId(Long id, String lang) {
        Optional<Cargo> byRequestId = cargoDAO.findByRequestId(id, lang);
        if(byRequestId.isPresent()){
            Cargo cargo = byRequestId.get();
            return CargoDto
                    .builder()
                    .width(cargo.getWidth())
                    .height(cargo.getHeight())
                    .depth(cargo.getDepth())
                    .weight(cargo.getWeight())
                    .cargoType(cargo.getCargoType())
                    .request(requestService.findById(cargo.getRequestId().getId(), lang))
                    .build();
        } else {
            return null;
        }
    }
}
