package com.epam.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Component
public class RequestByUserDto {
    private RequestInfoByUserDto requestInfoByUserDto;
    private CargoDto cargoDto;
}
