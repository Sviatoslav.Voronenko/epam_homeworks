package com.epam.dto;


import com.epam.entity.City;
import com.epam.entity.Status;
import com.epam.entity.Tariff;
import com.epam.entity.User;
import com.epam.util.TimeFormatter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RequestDto {
    private Long id;
    private LocalDateTime departure;
    private LocalDateTime receiving;
    private City cityTo;
    private City cityFrom;
    private String address;
    private Status status;
    private LocalDateTime dateOfCreation;
    private User user;
    private Tariff tariff;
    private BigDecimal cost;
    private LocalDateTime dateOfPayment;

    public String getDeparture() {
        return TimeFormatter.format(departure);
    }

    public String getReceiving() {
        return TimeFormatter.format(receiving);
    }

    public String getCityTo() {
        return cityTo.getName();
    }

    public String getCityFrom() {
        return cityFrom.getName();
    }

    public String getDateOfCreation() {
        return TimeFormatter.format(dateOfCreation);
    }

    public String getTariff() {
        return tariff.getName();
    }

    public String getDateOfPayment() {
        return TimeFormatter.format(dateOfPayment);
    }
}
