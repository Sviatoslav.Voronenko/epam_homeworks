package com.epam.dto;

import com.epam.entity.CargoType;
import com.epam.entity.Request;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CargoDto {
    private int weight;
    private int height;
    private int width;
    private int depth;
    private CargoType cargoType;
    private Request request;
}
