package com.epam.mapper;

import com.epam.entity.Cargo;
import com.epam.entity.CargoType;
import com.epam.entity.Request;
import com.epam.exception.DBException;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CargoMapper implements ResultSetMapper<Cargo> {
    @Override
    public List<Cargo> map(ResultSet resultSet) {
        List<Cargo> cargos = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Cargo cargo = new Cargo(
                        resultSet.getLong("id"),
                        resultSet.getInt("weight"),
                        resultSet.getInt("height"),
                        resultSet.getInt("width"),
                        resultSet.getInt("depth"),
                        new CargoType(
                                resultSet.getLong("cargotype_id"),
                                resultSet.getString("cargotype_name")),
                        Request.builder()
                                .id(resultSet.getLong("request_id"))
                                .build()
                );
                cargos.add(cargo);
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return cargos;
    }
}
