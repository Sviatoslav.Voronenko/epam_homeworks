package com.epam.mapper;

import com.epam.entity.City;
import com.epam.exception.DBException;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CityMapper implements ResultSetMapper<City> {
    @Override
    public List<City> map(ResultSet resultSet) {
        List<City> cities = new ArrayList<>();
        try {
            while (resultSet.next()) {
                City city = new City(
                        resultSet.getLong("id"),
                        resultSet.getString("name")
                );
                cities.add(city);
            }
        } catch (SQLException e) {
            throw new DBException(e);
        }
        return cities;
    }
}
