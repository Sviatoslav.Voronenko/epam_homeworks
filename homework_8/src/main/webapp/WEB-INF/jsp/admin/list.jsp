<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

</head>
<body>
<select>
    <c:forEach var="user" items="${users}">
        <option>
            <c:out value="${user}"/>
        </option>
    </c:forEach>
</select>
<br>
<select>
    <c:forEach var="city" items="${cities}">
        <option>
            <c:out value="${city.getName()}"/>
        </option>
    </c:forEach>
</select>
</body>
</html>