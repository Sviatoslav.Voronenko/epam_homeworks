<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/lang.jspf" %>
<%@ include file="/WEB-INF/jspf/head.jspf" %>

<link rel="stylesheet" href="resources/css/style.css">
<script type="text/javascript" src="resources/js/tableFilter.js"></script>
</head>
<body>
<div class="container">
    <c:choose>
        <c:when test="${requests != null}">
            <select class="btn btn-info dropdown-toggle" id="myInput" onchange="findByStatus(4)">
                <c:forEach var="status" items="${statuses}">
                    <option>
                        <c:out value="${status.getName()}"/>
                    </option>
                </c:forEach>
            </select>
            <button type="button" class="btn btn-outline-danger" onclick="reset()"><fmt:message
                    key="requests_jsp.btn.reset"/></button>
            <button class="btn btn-outline-primary"><a href="/home"><fmt:message key="order_list_jsp.btn.back"/></a>
            </button>
            <div>
                <table class="table table-bordered" id="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col"><fmt:message key="requests_jsp.table.head.departure"/></th>
                        <th scope="col"><fmt:message key="requests_jsp.table.head.receiving"/></th>
                        <th scope="col"><fmt:message key="requests_jsp.table.head.address"/></th>
                        <th scope="col"><fmt:message key="requests_jsp.table.head.status"/></th>
                        <th scope="col"><fmt:message key="requests_jsp.table.head.from_the_city"/></th>
                        <th scope="col"><fmt:message key="requests_jsp.table.head.to_the_city"/></th>
                        <th scope="col"><fmt:message key="requests_jsp.table.head.cost"/></th>
                    </tr>
                    </thead>
                    <tbody id="target">
                    <c:forEach var="request" items="${requests}">
                        <tr>
                            <td>
                                <a href="<c:url value="${pageContext.request.contextPath}/request/${request.getId()}" />"><c:out
                                        value="${request.getId()}"/></a></td>
                            <td>
                                <c:choose>
                                    <c:when test="${request.getDeparture() == 'null'}">
                                        <fmt:message key="requests_jsp.list.not_assigned"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="${request.getDeparture()}"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${request.getReceiving() == 'null'}">
                                        <fmt:message key="requests_jsp.list.not_assigned"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="${request.getReceiving()}"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>

                            <td>
                                <c:choose>
                                    <c:when test="${empty request.getAddress()}">
                                        <fmt:message key="requests_jsp.list.address"/>
                                    </c:when>
                                    <c:otherwise>
                                        <c:out value="${request.getAddress()}"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>

                            <td>
                                <c:out value="${request.getStatusName()}"/>
                            </td>
                            <td>
                                <c:out value="${request.getCityFrom()}"/>
                            </td>
                            <td>
                                <c:out value="${request.getCityTo()}"/>
                            </td>
                            <td>
                                <c:out value="${request.getCost()}"/>
                            </td>

                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:when>
        <c:otherwise>
            <fmt:message key="requests_jsp.no_orders"/>
            <button><a href="order"><fmt:message key="home_jsp.list.create_order"/></a></button>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>