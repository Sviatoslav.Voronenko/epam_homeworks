package com.epam.service;

import com.epam.dao.interfaces.UserDAO;
import com.epam.entity.User;
import com.epam.service.impl.UserServiceImpl;
import com.epam.service.interfaces.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Slf4j
public class UserServiceTest {

    private final UserDAO userDAO = mock(UserDAO.class);

    private final UserService ser = new UserServiceImpl(userDAO);

    private long id;

    private String email;

    private List<User> users;

    private User user;

    @Before
    public void setUp() {
        id = 99L;
        email = "qwer";
        user = User
                .builder()
                .id(id)
                .email(email)
                .password("1")
                .firstName(email)
                .build();

        users = new ArrayList<>();
        users.add(user);
        users.add(User
                .builder()
                .id(100L)
                .firstName("q")
                .email("q")
                .password("1")
                .build());
    }

    @Test
    public void shouldFindUserById() {
        User user = this.user;
        when(userDAO.findById(id)).thenReturn(Optional.of(user));


        User u = ser.findById(id);
        verify(userDAO).findById(id);


        assertThat(u, hasProperty("id", equalTo(id)));
    }

    @Test
    public void shouldFindUserByEmail() {
        User user = this.user;
        when(userDAO.findByEmail(email)).thenReturn(Optional.of(user));


        User u = ser.findByEmail(email);
        verify(userDAO).findByEmail(email);


        assertThat(u, hasProperty("email", equalTo(email)));
    }

    @Test
    public void shouldFindAllUsers() {
        List<User> users = this.users;
        when(userDAO.findAll()).thenReturn(users);

        Collection<User> foundUsers = ser.findAll();

        verify(userDAO).findAll();
        assertThat(foundUsers, hasSize(2));

        users.forEach(
                user -> assertThat(foundUsers, hasItem(allOf(
                        hasProperty("id", is(user.getId())),
                        hasProperty("firstName", is(user.getFirstName())),
                        hasProperty("email", is(user.getEmail())),
                        hasProperty("password", is(user.getPassword()))
                )))
        );
    }

    @Test
    public void shouldCreateUser() {
        User user = this.user;
        when(userDAO.create(any())).thenReturn(user);

        Map<String, String> request = new HashMap<>();
        request.put("name", email);
        request.put("email", email);
        request.put("password", "1");
        request.put("phone", "1");

        User foundUser = ser.create(request);
        foundUser.setId(id);

        verify(userDAO).create(any());
        assertThat(foundUser, hasProperty("email", equalTo(email)));
    }
}
