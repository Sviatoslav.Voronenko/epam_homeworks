package com.epam.controller;

import com.epam.AbstractBaseSpringTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@Sql("/init.sql")
public class ProfileControllerTestIT extends AbstractBaseSpringTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MockHttpSession mockHttpSession;


    private final Map<String, Object> sessAtr = new HashMap<>();


    @Before
    public void setUp() {
        mockHttpSession = new MockHttpSession(webApplicationContext.getServletContext());
        sessAtr.put("login", "email");
    }

    @Test
    public void getProfileShouldReturnProfileDetails() throws Exception {
        mockMvc.perform(
                get("/profile")
                        .accept(MediaType.TEXT_HTML)
                        .session(mockHttpSession)
                        .sessionAttrs(sessAtr))
                .andExpect(status().isOk());
    }
}
