package com.epam.controller;

import com.epam.entity.City;
import com.epam.entity.User;
import com.epam.service.interfaces.CityService;
import com.epam.service.interfaces.UserService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;

public class AdminListControllerTest {

    private final UserService userService = mock(UserService.class);

    private final CityService cityService = mock(CityService.class);

    private final AdminListController adminListController = new AdminListController(userService, cityService);

    private MockMvc mockMvc;

    @Before
    public void setup() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setSuffix(".jsp");

        mockMvc = MockMvcBuilders.standaloneSetup(adminListController)
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void shouldReturnAdminList() throws Exception {
        List<User> users = new ArrayList<>();
        users.add(User.builder().id(55L).build());
        users.add(User.builder().id(56L).build());

        List<City> cities = new ArrayList<>();
        cities.add(City.builder().id(55L).build());
        cities.add(City.builder().id(56L).build());

        given(userService.findAll()).willReturn(users);
        given(cityService.findAll(anyString())).willReturn(cities);

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.get("/adminPanel")
                        .accept(MediaType.TEXT_HTML)
                        .session(new MockHttpSession())
                        .sessionAttr("lang", "en"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attributeExists("cities"))
                .andReturn();

        List<User> modelUsers = (List<User>) mvcResult.getModelAndView().getModel().get("users");
        List<City> modelCities = (List<City>) mvcResult.getModelAndView().getModel().get("cities");

        users.forEach(user -> assertThat(modelUsers, hasItem(user)));
        cities.forEach(city -> assertThat(modelCities, hasItem(city)));
    }
}
