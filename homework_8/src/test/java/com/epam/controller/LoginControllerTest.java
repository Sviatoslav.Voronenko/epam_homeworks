package com.epam.controller;

import com.epam.entity.Role;
import com.epam.entity.User;
import com.epam.service.interfaces.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

    private final UserService userService = mock(UserService.class);

    private LoginController loginController = new LoginController(userService);

    private MockMvc mockMvc;

    private String email;

    private String password;

    private User user;

    @Before
    public void setUp() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/view/");
        viewResolver.setSuffix(".jsp");

        mockMvc = MockMvcBuilders.standaloneSetup(loginController)
                .setViewResolvers(viewResolver)
                .build();

        long id = 99L;
        email = "ad";
        password = "123";
        Role role = new Role();
        role.setName("ADMIN");
        user = User
                .builder()
                .id(id)
                .email(email)
                .password(password)
                .firstName(email)
                .role(role)
                .build();
    }

    @Test
    public void getLoginPageShouldReturnLoginPage() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get("/login")
                        .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }


    @Test
    public void loginUserShouldReturnHomePage() throws Exception {

        given(this.userService.findByEmail(anyString())).willReturn(user);

        mockMvc.perform(
                MockMvcRequestBuilders.post("/login")
                        .accept(MediaType.TEXT_HTML)
                        .param("name", email)
                        .param("password", password)
                        .session(new MockHttpSession())
                        .sessionAttr("lang", "en"))
                .andExpect(status().isFound());

    }
}
