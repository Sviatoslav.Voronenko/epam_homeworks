DROP TABLE IF EXISTS role;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role`
(
    id     int         NOT NULL AUTO_INCREMENT,
    `name` varchar(45) NOT NULL,
    PRIMARY KEY (id)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

INSERT INTO role
VALUES (1, 'admin');
INSERT INTO role
VALUES (2, 'user');

DROP TABLE IF EXISTS user;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user`
(
    id         int         NOT NULL AUTO_INCREMENT,
    first_name varchar(45) NOT NULL,
    last_name  varchar(45) DEFAULT NULL,
    email      varchar(45) NOT NULL,
    `password` varchar(45) NOT NULL,
    phone      varchar(15) NOT NULL,
    role_id    int         NOT NULL,
    PRIMARY KEY (id),
--   KEY fk_user_role1_idx (role_id),
    CONSTRAINT fk_user_role1 FOREIGN KEY (role_id) REFERENCES `role` (id) ON DELETE CASCADE
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

INSERT INTO user
VALUES (1, 'Alex', 'Lex', 'email', 'pass', '380', 2);
INSERT INTO user
VALUES (2, 'Admin', 'Admin', 'ad', '123', '000', 1);
INSERT INTO user
VALUES (3, 'Igor', NULL, 'igor@mail.com', 'password', '38066', 2);
INSERT INTO user
VALUES (4, 'Nick', NULL, 'Nick', 'Nick', 'Nick', 2);
INSERT INTO user
VALUES (5, 'Max', NULL, 'max@gmail.com', '12345', '38055', 2);
INSERT INTO user
VALUES (6, 'riflik', NULL, 'ok@gmail.com', '123', '33332222', 2);